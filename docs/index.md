## A propos de moi


| --- | --- |
|![Valentin]()|<ul><li> Je m’appelle Valentin étudiant en master 2 en module 3 de l'option AD. L'année passé en Master 1 j'ai eu l'occasion de faire l'atelier Digital fabrication Studio au Q1. Cet atelier m'a beaucoup plus car je suis très intéressé par tout ce qui est manuel et la création d’objet. De plus ce semestre a permis une opportunité pour découvrir de nouvelle machine assistée par ordinateur. Ce quadrimestre je souhaite donc continuer dans cette lancée et pouvoir approfondir le développement et le processus de création.  
A coté de mes études, je fait de la scénographie pour des événements et lightjokey ainsi que régisseur lumière pour des pièces de théâtre.

### 
### Derniers projets

#### Premier quadri 2019

L'an passé lors du Q1 j'ai travaillé sur la fabrication d'une machine  qui plante les patates. Celle-ci à été conçue avec que des matériaux de récupération comme des cadres de vélos, des barils d'huile en acier... Le fil conducteur de l'atelier était Cuba et le principe était d'utiliser les matériaux disponibles.

Lien du projet: [](https)

#### Deuxième quadri 2020

Au second quadri j'ai choisi de continuer dans cette lancée avec le FabLab mais dû au Covid l'accès aux machines ne fut plus possible, nous nous sommes alors concentrés sur l'approfondissement de logiciels informatiques paramétriques tels que SolidWorks/Grasshopper et leurs plug-ins. Malgré le fait que nous avons du apprendre à utiliser les logiciels chacun de son côté, c'était très intéressant. 

Lien du projet: []()

#### Choix de l'objet

L'objet que j'ai choisi est de petite dimension plutôt discrète et pratique car il peut se plier et de ce fait être rangé ou transporté facilement. Il s'agit d'un tabouret concentrique conçu par Patrick Jouin. Ce que j'ai trouvé intéressant dans cet objet c'est son aspect modulable révélant deux états : plié et déplié. A première vue son principe de fabrication et de modularité parait simple, mais ce n'est pas le cas!. Il s'agit d'une conception avec des lamelles de plastiques identiques, assemblées autour d'un élément central. Le mouvement pour effectuer son repli fait une trajectoire circulaire. Cela se fait grâce à des liaisons complexes entre chaque lamelle de plastique ainsi que le tube central étant l'élément majeur pour effectuer le repli. Grâce à ce principe de repli, le tabouret replié a une forme générale de tube plutôt mince. Toutes les lamelles sont plaquées à l'axe central (le tube).


![valentin](docs/images/thumb_.jpg)


ONESHOT - Patrick Jouin

![valentin](docs/images/Capture_d_écran_2020-10-28_180028.png)

### Le designer :
Patrick Jouin est un designer qui peut travailler aussi bien le design industriel que les arts décoratifs. Plusieurs de ses créations sont entrées dans les collections permanentes de musées dans le monde entier.
En 2004 il lance une série de meuble à échelle 1 réalisée grâce aux technologies de l'impression 3D. Le tabouret « One Shot » en fait partie. Il à été conçu en 2006.
Patrick Jouin effctue aussi beaucoup de projet de design d'intérieur comme des espaces commun/bar pour des hotels. Il travaille beaucoup avec les differents sens, t'elle que l'ouie/son et la vue/lumière. Sur la photo de gauche ils'agit du "Cabinet sonore de Patrick Jouin et Sanjit Manku" Une collection de son est disponible sur la partie avant dite "la bibliotheque sonore" puis à l'opposé un sofa.
La photo de droite est un espace commun dans un hotel, on peut observer qu'il y à un travaille sur les differentes matière. 

![valentin](docs/images/A1.png)

Patrick Jouin s'intéresse aussi au objet du quotidient il l'ai revisite, les optimise. Ou leur rajoute une fonction. Comme la spatule à nuttella avec une encoche, celle ci évite que la spatule tombe dans le peau. Ou la cuillère qui vien ce placer dans le manche de la casserole de cette pçon la cuillère peut y etre ranger puis lors de l'utilisation elle peut etre reposé a cette emplacment pour évité qu'elle tombe à l'interieur de la casserole 


![valentin](docs/images/A2.png)

En 2004 il lance une série de meuble à échelle 1 réalisée grâce aux technologies de l'impression 3D avec la technologie Laser. Il réalise plusieurs objets utilisé quotidiennement t'elle que des chaises, une tables, une lampe. La fabrication de c'est objet a été faite en un bloc avec qu'une seul matière. Avec une impriment standar à filament cela n'aurai pas était possible du fait de plusieur contrainte t'elle que la dimension, la solidité, la technique. Ce que je trouve intérésant dans c'est réalisation c'est la démarche d'utiliser cette technologie (l'impression 3D avec la technologie Laser) pour réaliser du mobilier complet et utilisable!


![valentin](docs/images/A3.png)


### Le principe :
Le principe est basé sur un système de module identique. Il y a 2 modules de formes différentes : les modules d'assise ainsi que les modules de pied. Ces deux modules ont une forme très complexe cela est du au système de repli concentrique. Le bute étant que, plié, le tabouret prenne beaucoup moins de place. La forme du tabouret replié est un tube cylindrique.

![valentin](docs/images/A4.png)

Le tabouret est composé de 12 lamelles de longue dimension qui ont le rôle de pied. Puis de 12 lamelles avec une surface plane, qui est la surface sur laquelle on s’assoit. Pour relier et permettre le repli du tabouret  toutes les lames de grande et de petite dimension son reliées par un élément central. Cet élément central a une forme de tube. Ce tube se rallonge grâce à l’emboîtement d'un autre tube plus petit situé à l’intérieur.

![valentin](docs/images/Capture_d_écran_2020-10-22_111323.png) 


### Fabrication :
Le tabouret One shot à été réalisé entièrement par une imprimante 3D à rayon laser. Sans aucune intervention humaine sauf pour la modélisation faite par un logiciel de modélisation 3D. Cette technologie n'est pas récente contrairement à ce que l'on pourrait penser. Cela étant probablement du à la démocratisation assez récente des imprimentes 3D à filament.
Ce procedé d'impression 3D (Frittage Sélectif par Laser, ou SLS (Selective Laser Sintering)) existe depuis 1988, le brevet à été déposé par Carl Deckard. 

Histoire des differents procedés d'impression : Les premiers essais d’impression 3D ont été réalisé par le Docteur Kodama. Il a développé une méthode de prototypage rapide en 1980. Il fut le premier à décrire une approche de la production couche par couche, créant ainsi un ancêtre de la Stéréolithographie (SLA). Mais le brevet à été déposé quelque temps après par Charles Hull en 1986. Ce type d'impression est réalisé grâce à de la résine, par la suite je détaillerai son principe de fonctionnement. 
Ensuite en 1988, l'impression 3D laser par frittage est apparu. Puis peu après un brevet à été déposé par Scott Crump en 1992 pour la technologie par dépot de fil. C'est celle-ci que nous utilisons au fablab. 

![valentin](docs/images/chrono.png)


### Principe du Frittage sélectif par laser :

La technologie laser consiste à effectuer un frittage sélectif par laser. Plus précisement le frittage sélectif par laser permet d’imprimer des objets fonctionnels sans recourir à un liant intermédiaire ou à une éventuelle étape d’assemblage. 

Le passage du laser sur la couche de poudre, provoque une réaction qui fait que la poudre va se solidifier aux endroits ou le laser a pointé. Au passage du laser la poudre se consolide.
Plus précisement:
Un dépot de poudre d'une mince épaisseur est déposée grâce au passage d'un rouleau. Celle-ci est déposée regulièrement avant le passage du laser. Le laser procède par couche. Donc la solidification de la poudre se fait couche par couche.
De ce fait, ce procédé constitue un énorme avantage par rapport aux autres procedés d'impression 3D car il n'y a pas besoin de support!! Donc des formes très complexes sont réalisables facilement.

![valentin](docs/images/P22.png)

Différentes matières peuvent être utilisées pour utiliser ce procéder tels que : les polymères plastiques en tous genre. Le plus commun est le polyamide ( Pa 12)

![valentin](docs/images/B12.png)

Voici ce que l'on obtient à la fin de l'impression. Il s'agit d'un cube de poudre, à l'intérieur se trouve l'objet. Il faut donc procéder a un nettoyage, il faut extraire toute la poudre à l'aide d'une sorte de pinceaux et d'un aspirateur. Pour à la fin obtenir son objet.

![valentin](docs/images/B13.png)



Ce procedé technologique est aussi utilisé pour modéliser des pièces en métal nommé DMLS (Direct Metal Laser Sintering). La différence réside dans le type de poudre. Ce procédé permet de réaliser des objets en métal en fusionnant cette fois une poudre de fines particules métalliques.

![valentin](docs/images/SLM.jpg)

### Le mode d'assemblage :
Le mode d'assemblage est très particulier. Du fait de l'utilisation de l'impression 3D sur la totalité du processus de fabrication. Cela permet d’éviter l'ajout de tout élément de liaison tels que des essieus, vis, ressorts, gonds...
La totalité des éléments de liaison est donc faite dans la masse. Tout est de la même matière.

![valentin](docs/images/A6.png)


### Matériaux :
Polyamide (SLS)
Les principales technologies d'impression 3D basées sur de la poudre sont le frittage sélectif par laser (connu sous le nom SLS pour Selective Laser Sintering) et la fusion sélective par laser (ou SLM, pour Sélective Laser Melting). 
Dans les vêtements, le polyamide est couplé avec du coton , de la laine ou autre pour avoir de nouvelles propriétés telles que l'étanchéité, la solidité et élasticité... 

![valentin](docs/images/B7.png)

Ces matériaux ainsi que ce procédé permet de réaliser des objets mécaniques très complexe tels qu'un opturateur, ou un vélo. Ces deux objets ont été réalisé en une seule impression! 

![valentin](docs/images/B9.png)

Quelques exemples d'objets du quotidient dans lequel est utilisé le polyamide, maintenant  plus communémant appelé Nylon.
Il est très souvant couplé avec d'autres matériaux tel que le tissu. Il procure de nombreux avantages au tissu tels que l'étanchéité ainsi que l'élasticité. 

![valentin](docs/images/B8.png)

On peut voir sur ces images qu'il est possible de créer des objets tel que du mobilier avec des reliefs très très complexe comme ce lustre ou cette chaise.  

![valentin](docs/images/111.png)

### Choix de l'imprimante 3D résine :

Comme décrit précedement, il existe une imprimante 3d à resine; celle-ci contrairement à l'imprimante 3D par frittage laser qui existe que en version industrielle, est commercialisée en differentes versions et en petite taille pour les particuliers.
De ce fait, je vais mettre en comparaison ces deux imprimantes 3d en énonçant leurs avantages et inconvéniants    :   

Les avantages :
*La matière utilisable : Une multitude de résine existe pour effectuer ce type d'impression. Des résines de couleur, des résines transparentes et même des résines avec différentes propriétés de solidité. Il est tout à fait possible de réaliser des objets souples, un peu comme du silicone. 
De la résine utilisable dans l'alimentaire existe aussi comme par exemple pour la production de moule pour les dentistes.

Les inconvéniants : 
La création de support est obligatoire contrairement aux impressions 3D à frittage laser qui n'en nécéssitent pas du fait de leurs procedés.

Les points communs avec une imprimante 3D par frittage 

L'imprimante 3D à resine a de nombreux points communs avec l'impression 3D par frittage, au niveau du résultat des pièces ainsi que de par leurs "propriètés" .

### Détail sur son fonctionnement :

![valentin](docs/images/p11.png)

*Son fonctionnent :
Il existe globalement trois types d'impressions 3D qui utilisent le principe de Stéréolithographie. C'est le moyen de solidification de la résine qui diffère. Le type de projection.

*Laser : c'est directement un laser qui passe par un réseau de miroir et qui arrive sur la resine et la solidifie. L'inconvéniant c'est son temps d'impression car le laser est juste un pointeur donc il doit tout dessiner pour chaque couche sur la couche de résine.

*DLP : il s'agit d'une projection conique de la lumière UV donc elle couvre toute une surface d'un coup. Pour que ce soit uniquement l'image de la couche qui soit projetetée sur la résine, les rayons sont selectionés par un ecran LCD qui empêche à certains endroits de passer. 

*LED : Il s'agit d'une plateforme remplie de led UV. Celle-ci projette aussi sur un ecran LCD. Le principe est le même que pour la technologie DLP. Il y a un avantage, c'est que la déformation sur les extrémités de la pièce n'existe pas contrairement à la projection conique produit par la technologie DLP

![valentin](docs/images/okok.png)

L'imprimante 3D DLP :
Un rayon UV est projeté sur la surface d'un ecran LCD 2K 4K selon sa précision. L'écran permet de faire passer ou non les rayons UV à travers la surface. Les rayons UV qui ont traversé l'ecran vont donc être en contact avec la couche de résine, celle-ci va donc se solidifer. Une image sur l'écran represente une couche de résine solidifiée. Donc le plateau supérieur sur lequel est collée la forme s'élève en conséquence couche par couche. La forme est donc réalisée à l'envers.

![valentin](docs/images/roue1.png)


![valentin](docs/images/1000.png)

![valentin](docs/images/110.png)


![valentin](docs/images/105.png)


![valentin](docs/images/112.png)


![valentin](docs/images/3000.png)


**MODELISATION D'EMSEMBLE V0.1**



![valentin](docs/images/C2.png)

![valentin](docs/images/C3.png)

![valentin](docs/images/C1.png)

![valentin](docs/images/print_3D_2.png)

mettre photo de l'impression 

**ETUDE FORME DES TIGES DE PLASTIQUE**

![valentin](docs/images/Section.png)

![valentin](docs/images/AT10.png)

mettre photo de l'impression

**TYPES DE REMPLISSAGE**

![valentin](docs/images/TestR.png)

![valentin](docs/images/RR.png)

Variation angle du remplissage 

![valentin](docs/images/RANGLE.png)

Variation du nombre de couche de la parois 

![valentin](docs/images/CCOUCHE.png)



**LES TESTS D'ATTACHE 0.1**

![valentin](docs/images/ATT1.png)

![valentin](docs/images/ATA.png)

**LES TESTS D'ATTACHE 0.2**

![valentin](docs/images/022.png)

![valentin](docs/images/ATT22.png)

mettre photo de l'impression 

**MODELISATION D'EMSEMBLE V0.2**

![valentin](docs/images/AAA.png)

![valentin](docs/images/BBBB.png)

**Détaille assemblage V0.2**

*Liaison Assise*

![valentin](docs/images/L100.png)

*Liaison pied avant*

![valentin](docs/images/P1000.png)

![valentin](docs/images/P200.png)

*Liaison pied avant/tige base*

![valentin](docs/images/M100.png)




**Les esquisses :**

![valentin](docs/images/Presentation_2AD.png)


**Prototype final - Les composants :**

![valentin](docs/images/Presentation_3_AD.png)

**Fabrication des composants - CNC vertical/imprimente 3D SLA :**
