## Journal de Bord

### Jeudi 1 Octobre 2020



### Jeudi 8 Octobre 2020



## Uni Sign Pen 
### Design et fabrication

Fabriqués en France dans l’usine de Bry-sur-Marne (94), la seule usine d’Europe.
Encre sans solvants nocifs.

_**Matière**_ : polystyrène cristal/Polyèthylène

Le polystyrène cristal :
Le PS est un thermoplastique transparent avec une structure amorphe. Il est particulièrement utilisé pour la vaisselle jetable, les éléments décoratifs, le boitage économique… 

Point fort                
-  Bonne rigidité mais cassant 
-  Bel aspect brillant         
-  Surface dure de bel aspect  
-  Coût réduit                 
 
Point faible                      
-  Mauvaise tenue température       
-  Cassant                            
-  Très mauvaise résistance chimique 
-  Electrostatique|

Polyèthylène basse densité :
Le PEBD est un thermoplastique souple semi-cristallin. Il est notamment utilisé pour la fabrication de film, de tuyau souple, de jouet, de flacon souple.

Point fort                
-  Meilleure rigidité 
-  Non cassant         
-  Anti-adhérent 
-  Tenue chimique                
 
Point faible                      
-  Mauvaise tenue température     

_***Procéder de fabrication :***_


![injection](proceder_de_fabrication.png)

Injection sous pression de bille de plastique dans un moule en aluminium.
Les billes sont fondu à haute température elle deviennent ensuite liquides.


_***Assemblage :***_

Nombre de pièce :
Trois pièces 
1. Bouchon avec accroche
1. Tube fuseler principale 
1. Bouchon pour fermer le tube fuseler

Légende:
- Vert = Polyèthylène basse densité
- Bleu = Polystyrène cristal


! [Schema](Capture_d_écran_2020-10-21_191743.png)

### Contexte de création
### Influence culturelle

Le Japon porte un très grand intérêt à l’écriture depuis le 5 ème siècle lors de son développement. Celle-ci se composant d’idéogrammes en partie inspirés de Chine et de deux types d’écritures propres au Japon, elle est intéressante d’un point de vue de l’art qui se cache derrière la représentation ainsi que la signification des symboles. 

Au début, plus pratiquée par les nobles et ensuite par les samouraïs, la représentation de ces symboles, la calligraphie japonaise qui dans sa traduction *shodo* signifiant “la voie de l’écriture” renvoie à la religion bouddhiste et à un aspect spirituel de l’art d’écrire à la main qui représente davantage un art de vivre qui maintient l'esprit et le corps qu’un art purement esthétique. (Certains moines l’utilisent pour de la méditation)

Aujourd’hui cet art est intégré dès le plus jeune âge dans les écoles japonaises, et malgré la difficulté de cet apprentissage, le taux d'illettrisme au Japon est infiniment peu élevé et c’est spécifiquement l’emploi d’outils manuels qui justifierait cela selon les puristes japonais, le passage du dessin à la main apporte beaucoup à la compréhension des symboles.

Pour cet art, 4 outils primordiaux sont nécessaires (4 trésors du calligraphe):

![influence culturelle](docs/images/docs_images_influence_culturelle_Sign_Pen.png)

La préparation de l’encre est une étape nécessaire à la préparation de l’esprit, néanmoins de nos jours, l’emploi de stylo ou du feutre devient de plus en plus répandu pour la calligraphie. 

Ces outils représentent un format portable plus compact de plusieurs éléments, ils peuvent être transportés dans une poche et représentent un gain de temps de préparation vu qu’ils sont prêts à l’emploi directement.

![influence culturelle 2](docs/images/docs_images_influence_culturelle_2.png)

Outre l’art de la calligraphie, les mangas représentent un secteur très prisé notamment à l’étranger, la pratique de cet art nécessite l’emploi de divers instruments d’écriture tel que les feutres. Le Japon excelle dans les stylos et feutres avec une spécialisation dans la pointe fine permettant aussi divers épaisseurs et arrondis.

Le sign pen semble reprendre les caractéristiques de base nécessaire à la calligraphie japonaise, de l’encre et de l’eau. Cela permet de rendre l’encre résistante à l’eau et donc au temps et donc inaltérable. Néanmoins elle ne possède pas une brosse flexible ce qui lui permet donc de créer peu de variantes dans les épaisseurs mais celle-ci ne laisse pas l’encre transpercer le papier.

### Test et avis 

Nous avons testé le feutre sign pen sous différents angles. Celui-ci permet de jouer avec les épaisseurs de traits en fonction de son inclinaison. De cette manière, nous pouvons tracer des lignes relativement fines ainsi que des traits plus marque.
Il est important de noter que les différents tests ont été réalisé sur du papier 300g à grains fort. Les traits sont donc peu régulidés et nous pouvons voir que l’écoulement de l’encre n’est parfois pas suffisant. 

![Test Papier 300g](docs/images/docs_images_test_papier_300g.png)

En revanche, lorsque le marqueur est utilisé sur une surface plus lisse, plus régulière, ici un support cartonné brillant, les traits sont beaucoup plus précis et les lignes plus régulières. En effet le carton blanc absorbe moins d’encre par rapport au papier 300 grammes. 

![Test Carton lisse](docs/images/docs_images_test_carton_blanc.png)

Nous l’avons également soumis à un test de dessin de perspective. Ce dessin a été réalisé uniquement avec le Sign Pen Uni. Il s’agit donc d’un feutre polyvalent utile pour tracer des lignes franches et à la fois des détails beaucoup plus fins.

![Test dessin perspective](docs/images/docs_images_test_sketch_sign_pen.png)

De manière générale, nous constatons après test que le sign pen uni est un bon outil de dessin, principalement lorsqu’il s’agit de tracer des lignes et des écritures. Il ne se prête pas aux grands aplats ainsi qu'a toutes les surfaces de dessin en raison du flux d'encre limité, même si cela peut etre un un atout pour un effet de style.








### Jeudi 15 Octobre 2020
